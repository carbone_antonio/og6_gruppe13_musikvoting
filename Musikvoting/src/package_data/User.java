package package_data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Statement;
import java.util.ArrayList;

import package_logic.Teilnehmer;

public class User
{
	public static ArrayList<Teilnehmer> getUsers()
	{

		java.sql.Connection con = Connection.getConnection();
		ArrayList<Teilnehmer> teilnehmerList = new ArrayList<>();

		try
		{
			Statement stmt = con.createStatement();
			String sql = "Select P_idTeilnehmer,Titel,Name,Haus From teilnehmer;";
			ResultSet rst = stmt.executeQuery(sql);

			while (rst.next())
			{
				Teilnehmer teilnehmer = new Teilnehmer(rst.getInt("P_idTeilnehmer"), rst.getString("Titel"),
						rst.getString("Name"), rst.getString("Haus"), 0);
				teilnehmerList.add(teilnehmer);
			}

		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return teilnehmerList;

	}

	public static void addUser(Teilnehmer teilnehmer)
	{

		java.sql.Connection con = Connection.getConnection();

		try
		{
			Statement stmt = con.createStatement();
			String sql = "INSERT INTO `musikvoting`.`Teilnehmer` (`P_idTeilnehmer`, `Titel`, `Name`, `Haus`, `isGastgeber`)\r\n" + 
					"VALUES (DEFAULT,"+teilnehmer.getTitel()+","+teilnehmer.getName()+","+teilnehmer.getHaus()+", '0');";
			ResultSet rst = stmt.executeQuery(sql);
			
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
