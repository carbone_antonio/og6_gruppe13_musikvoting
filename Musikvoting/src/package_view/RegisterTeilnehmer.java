package package_view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import package_data.User;
import package_logic.Teilnehmer;

import java.awt.GridLayout;
import java.awt.GridBagLayout;
import javax.swing.JTextField;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JButton;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class RegisterTeilnehmer extends JFrame
{

	private JPanel contentPane;
	private JTextField txt_titel;
	private JTextField txt_name;
	private JTextField txt_haus;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					RegisterTeilnehmer frame = new RegisterTeilnehmer();
					frame.setVisible(true);
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RegisterTeilnehmer()
	{
		setTitle("Register new Teilnehmer");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 318, 210);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		txt_titel = new JTextField();
		txt_titel.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				txt_titel.selectAll();
			}
		});
		txt_titel.setText("TITEL");
		GridBagConstraints gbc_txt_titel = new GridBagConstraints();
		gbc_txt_titel.weighty = 0.5;
		gbc_txt_titel.weightx = 1.0;
		gbc_txt_titel.gridwidth = 2;
		gbc_txt_titel.insets = new Insets(5, 0, 5, 0);
		gbc_txt_titel.fill = GridBagConstraints.BOTH;
		gbc_txt_titel.gridx = 0;
		gbc_txt_titel.gridy = 0;
		contentPane.add(txt_titel, gbc_txt_titel);
		txt_titel.setColumns(10);
		
		txt_name = new JTextField();
		txt_name.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				txt_name.selectAll();
			}
		});
		txt_name.setText("NAME");
		GridBagConstraints gbc_txt_name = new GridBagConstraints();
		gbc_txt_name.weighty = 0.5;
		gbc_txt_name.weightx = 1.0;
		gbc_txt_name.gridwidth = 2;
		gbc_txt_name.insets = new Insets(5, 0, 5, 0);
		gbc_txt_name.fill = GridBagConstraints.BOTH;
		gbc_txt_name.gridx = 0;
		gbc_txt_name.gridy = 1;
		contentPane.add(txt_name, gbc_txt_name);
		txt_name.setColumns(10);
		
		txt_haus = new JTextField();
		txt_haus.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				txt_haus.selectAll();
			}
		});
		txt_haus.setText("HAUS");
		GridBagConstraints gbc_txt_haus = new GridBagConstraints();
		gbc_txt_haus.weighty = 0.5;
		gbc_txt_haus.weightx = 1.0;
		gbc_txt_haus.gridwidth = 2;
		gbc_txt_haus.insets = new Insets(5, 0, 5, 0);
		gbc_txt_haus.fill = GridBagConstraints.BOTH;
		gbc_txt_haus.gridx = 0;
		gbc_txt_haus.gridy = 2;
		contentPane.add(txt_haus, gbc_txt_haus);
		txt_haus.setColumns(10);
		
		JButton btn_registriere = new JButton("Registrieren");
		btn_registriere.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				User.addUser(new Teilnehmer(0, txt_titel.getText(), txt_name.getText(), txt_haus.getText(), 0));
				dispose();
			}
		});
		GridBagConstraints gbc_btn_registriere = new GridBagConstraints();
		gbc_btn_registriere.weightx = 0.5;
		gbc_btn_registriere.insets = new Insets(5, 0, 0, 0);
		gbc_btn_registriere.fill = GridBagConstraints.BOTH;
		gbc_btn_registriere.weighty = 0.5;
		gbc_btn_registriere.gridx = 1;
		gbc_btn_registriere.gridy = 3;
		contentPane.add(btn_registriere, gbc_btn_registriere);
	}


}
