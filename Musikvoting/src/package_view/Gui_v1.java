package package_view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import package_data.User;
import package_logic.Teilnehmer;

import java.awt.CardLayout;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.Font;
import java.awt.GridBagLayout;

import javax.swing.SwingConstants;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import java.awt.Insets;
import javax.swing.BoxLayout;

public class Gui_v1 extends JFrame
{

	private JPanel contentPane;
	private JTextField textField;
	private JScrollPane pnllist;
	private ArrayList<Teilnehmer> teilnehmerList = User.getUsers();
	private JComboBox cbx_setup_selectGastgeber;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					Gui_v1 frame = new Gui_v1();
					frame.setVisible(true);
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Gui_v1()
	{		
		setTitle("Musikvoting");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel container = new JPanel();
		contentPane.add(container, BorderLayout.CENTER);
		container.setLayout(new CardLayout(0, 0));
		
		CardLayout cl = (CardLayout)(container.getLayout()); //WICHTIG
		
		
		
		JPanel pnl_setup = new JPanel();
		container.add(pnl_setup, "setup");
		pnl_setup.setLayout(new GridLayout(5, 2, 20, 10));
		
		JLabel lbl_setup = new JLabel("Setup");
		lbl_setup.setHorizontalAlignment(SwingConstants.LEFT);
		lbl_setup.setFont(new Font("Tahoma", Font.BOLD, 15));
		pnl_setup.add(lbl_setup);
		
		JLabel lbl_leer = new JLabel("");
		pnl_setup.add(lbl_leer);
		
		JButton btn_setup_resetDB = new JButton("Reset DB");
		pnl_setup.add(btn_setup_resetDB);
		
		JLabel lbl_leer2 = new JLabel("");
		pnl_setup.add(lbl_leer2);
		
		cbx_setup_selectGastgeber = new JComboBox();
		cbx_setup_selectGastgeber.setModel(new DefaultComboBoxModel(new String[] {"Gastgeber"}));
		pnl_setup.add(cbx_setup_selectGastgeber);
		
		fillComboBox();
		
		JButton btn_setup_RegisterTeilnehmer = new JButton("Register Teilnehmer");
		btn_setup_RegisterTeilnehmer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new RegisterTeilnehmer().setVisible(true);
			}
		});
		pnl_setup.add(btn_setup_RegisterTeilnehmer);
		
		JLabel lbl_leer1 = new JLabel("");
		pnl_setup.add(lbl_leer1);
		
		JButton btn_setup_start = new JButton("Start");
		btn_setup_start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setSize(270,160);
				cl.show(container, "login");
			}
		});
		pnl_setup.add(btn_setup_start);
		
		JPanel pnl_login = new JPanel();
		container.add(pnl_login, "login");
		GridBagLayout gbl_pnl_login = new GridBagLayout();
		gbl_pnl_login.columnWidths = new int[]{0, 0, 0};
		gbl_pnl_login.rowHeights = new int[]{0, 0, 0, 0};
		gbl_pnl_login.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		gbl_pnl_login.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		pnl_login.setLayout(gbl_pnl_login);
		
		JLabel lblNewLabel = new JLabel("Login");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 15));
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel.insets = new Insets(10, 0, 10, 0);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		pnl_login.add(lblNewLabel, gbc_lblNewLabel);
		
		textField = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 10, 0);
		gbc_textField.weighty = 0.5;
		gbc_textField.weightx = 1.0;
		gbc_textField.gridwidth = 2;
		gbc_textField.fill = GridBagConstraints.BOTH;
		gbc_textField.gridx = 0;
		gbc_textField.gridy = 1;
		pnl_login.add(textField, gbc_textField);
		textField.setColumns(10);
		
		JButton btn_register = new JButton("Register");
		btn_register.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new RegisterTeilnehmer().setVisible(true);
			}
		});
		GridBagConstraints gbc_btn_register = new GridBagConstraints();
		gbc_btn_register.insets = new Insets(0, 0, 10, 0);
		gbc_btn_register.weighty = 1.0;
		gbc_btn_register.weightx = 1.0;
		gbc_btn_register.ipadx = 1;
		gbc_btn_register.gridx = 0;
		gbc_btn_register.gridy = 2;
		pnl_login.add(btn_register, gbc_btn_register);
		
		JButton btn_login = new JButton("Login");
		GridBagConstraints gbc_btn_login = new GridBagConstraints();
		gbc_btn_login.insets = new Insets(0, 0, 10, 0);
		gbc_btn_login.weighty = 1.0;
		gbc_btn_login.weightx = 1.0;
		gbc_btn_login.gridx = 1;
		gbc_btn_login.gridy = 2;
		pnl_login.add(btn_login, gbc_btn_login);
		
		JPanel pnl_vote = new JPanel();
		container.add(pnl_vote, "vote");
		pnl_vote.setLayout(new BorderLayout(0, 0));
		
		//WICHTIG
		JPanel pnl_list = new JPanel(new GridLayout(0, 1, 0, 0));
		pnl_vote.add(new JScrollPane(pnl_list));
		
		
		JPanel pnl_buttons = new JPanel();
		pnl_vote.add(pnl_buttons, BorderLayout.SOUTH);
		pnl_buttons.setLayout(new GridLayout(0, 3, 0, 0));
		
		JButton btn_addsong = new JButton("Add Song");
		btn_addsong.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JPanel panel = new JPanel();
				
				GridBagLayout gbl_contentPane = new GridBagLayout();
				gbl_contentPane.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
				gbl_contentPane.rowHeights = new int[]{0, 0};
				gbl_contentPane.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
				gbl_contentPane.rowWeights = new double[]{0.0, Double.MIN_VALUE};
				
				panel.setLayout(gbl_contentPane);
				
				JLabel lblNewLabel = new JLabel("New label");
				GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
				gbc_lblNewLabel.anchor = GridBagConstraints.WEST;
				gbc_lblNewLabel.weightx = 1.0;
				gbc_lblNewLabel.gridwidth = 7;
				gbc_lblNewLabel.insets = new Insets(0, 0, 0, 5);
				gbc_lblNewLabel.gridx = 0;
				gbc_lblNewLabel.gridy = 0;		
				
				JButton btnNewButton = new JButton("New button");
				GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
				gbc_btnNewButton.gridx = 8;
				gbc_btnNewButton.gridy = 0;
				
				panel.add(lblNewLabel, gbc_lblNewLabel);
				panel.add(btnNewButton, gbc_btnNewButton);
				
				pnl_list.add(panel);
                
                validate();
                repaint();
			}
		});
		pnl_buttons.add(btn_addsong);
		
		JLabel lbl_leer3 = new JLabel("");
		pnl_buttons.add(lbl_leer3);
		
		JButton btn_finish = new JButton("Finish");
		pnl_buttons.add(btn_finish);
		
		
		
		cl.show(container, "setup");
	}
	
	@SuppressWarnings("unchecked")
	public void fillComboBox() {
		cbx_setup_selectGastgeber.removeAllItems();
		for(int i = 0; i < teilnehmerList.size(); i++)
			cbx_setup_selectGastgeber.addItem(new ComboItem(teilnehmerList.get(i).getName(), teilnehmerList.get(i).getName()));
		
	}
	
	class ComboItem
	{
	    private String key;
	    private String value;

	    public ComboItem(String key, String value)
	    {
	        this.key = key;
	        this.value = value;
	    }

	    @Override
	    public String toString()
	    {
	        return key;
	    }

	    public String getKey()
	    {
	        return key;
	    }

	    public String getValue()
	    {
	        return value;
	    }
	}

}
