package package_logic;

public class Song
{
	private int song_id;
	private String titel;
	private String interpret;
	private String genre;

	public Song()
	{

	}

	public Song(String titel, String interpret, String genre)
	{
		super();
		this.titel = titel;
		this.interpret = interpret;
		this.genre = genre;
	}

	public Song(int song_id, String titel, String interpret, String genre)
	{
		super();
		this.song_id = song_id;
		this.titel = titel;
		this.interpret = interpret;
		this.genre = genre;
	}

	// Getters and Setters
	public String getTitel()
	{
		return titel;
	}

	public void setTitel(String titel)
	{
		this.titel = titel;
	}

	public String getInterpret()
	{
		return interpret;
	}

	public void setInterpret(String interpret)
	{
		this.interpret = interpret;
	}

	public String getGenre()
	{
		return genre;
	}

	public void setGenre(String genre)
	{
		this.genre = genre;
	}

	public int getSong_id()
	{
		return song_id;
	}

	public void setSong_id(int song_id)
	{
		this.song_id = song_id;
	}

}
