package package_logic;

public class Teilnehmer
{
	private int teilnehmer_id;
	private String titel;
	private String name;
	private String haus;
	private int isGastgeber;

	public Teilnehmer()
	{
		super();
	}

	public Teilnehmer(String titel, String name, String haus, int isGastgeber)
	{
		super();
		this.titel = titel;
		this.name = name;
		this.haus = haus;
		this.isGastgeber = isGastgeber;
	}

	public Teilnehmer(int teilnehmer_id, String titel, String name, String haus, int isGastgeber)
	{
		super();
		this.teilnehmer_id = teilnehmer_id;
		this.titel = titel;
		this.name = name;
		this.haus = haus;
		this.isGastgeber = isGastgeber;
	}

	// Getters and Setters
	public String getTitel()
	{
		return titel;
	}

	public void setTitel(String titel)
	{
		this.titel = titel;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getHaus()
	{
		return haus;
	}

	public void setHaus(String haus)
	{
		this.haus = haus;
	}

	public int getTeilnehmer_id()
	{
		return teilnehmer_id;
	}

	public void setTeilnehmer_id(int teilnehmer_id)
	{
		this.teilnehmer_id = teilnehmer_id;
	}

	public int getIsGastgeber()
	{
		return isGastgeber;
	}

	public void setIsGastgeber(int isGastgeber)
	{
		this.isGastgeber = isGastgeber;
	}
}
