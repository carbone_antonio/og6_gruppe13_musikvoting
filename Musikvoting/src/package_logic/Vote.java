package package_logic;

public class Vote
{
	private int teilnehmer_id;
	private int song_id;
	
	public Vote(int teilnemer_id, int song_id)
	{
		super();
		this.teilnehmer_id = teilnemer_id;
		this.song_id = song_id;
	}

	public Vote()
	{
		super();
	}

	public int getTeilnehmer_id()
	{
		return teilnehmer_id;
	}

	public void setTeilnemer_id(int teilnemer_id)
	{
		this.teilnehmer_id = teilnemer_id;
	}

	public int getSong_id()
	{
		return song_id;
	}

	public void setSong_id(int song_id)
	{
		this.song_id = song_id;
	}

}
